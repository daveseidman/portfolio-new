import onChange from 'on-change';
import { createEl, addEl } from 'lmnt';
import Scene from './modules/scene';
import './index.scss';

class App {
  constructor() {
    const state = {
      location: null,
    };

    this.state = onChange(state, this.update);

    this.scene = new Scene();
    this.el = createEl('div', { className: 'app' });
    addEl(this.el, this.scene.el);
  }

  update(path, prev, next) {
    console.log(path, prev, next);
  }
}

const app = new App();
addEl(app.el);
if (window.location.hostname === 'localhost') window.app = app;

import { Scene as SceneThree, WebGLCubeRenderTarget, ReinhardToneMapping, WebGLRenderer, PerspectiveCamera, TextureLoader, SphereGeometry, BoxGeometry, HemisphereLight, Color, BackSide, MeshBasicMaterial, MeshNormalMaterial, Mesh } from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader';

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';


import autoBind from 'auto-bind';

export default class Scene {
  constructor(state) {
    autoBind(this);
    this.state = state;
    this.scene = new SceneThree();
    this.renderer = new WebGLRenderer({ antialias: true });
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.shadowMap.enabled = true;
    this.renderer.toneMapping = ReinhardToneMapping;// CineonToneMapping;// ReinhardToneMapping;// LinearToneMapping;
    // this.renderer.toneMappingExposure = 2.25;
    this.el = this.renderer.domElement;
    this.camera = new PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 1500);
    this.controls = new OrbitControls(this.camera, this.el);
    // this.controls.enablePan = false;
    // this.controls.enableZoom = false;
    this.controls.enableDamping = true;
    const geometry = new BoxGeometry();
    const material = new MeshNormalMaterial();
    this.cube = new Mesh(geometry, material);
    // this.scene.add(this.cube);

    this.camera.position.z = 5;
    // this.background = new Mesh(new SphereGeometry(1000, 16, 16), new MeshBasicMaterial({ map: new TextureLoader().load('assets/images/env1.jpg'), side: BackSide }));
    // this.scene.add(this.background);

    new TextureLoader().load('client/assets/images/env1.jpg', (texture) => {
      const rt = new WebGLCubeRenderTarget(texture.image.height);
      rt.fromEquirectangularTexture(this.renderer, texture);
      this.environment = rt.texture;
      this.scene.background = this.environment;
      this.scene.environment = this.environment;
    });


    // this.light = new HemisphereLight(0xffffbb, 0x080820, 1);
    // this.scene.add(this.light);

    new GLTFLoader().load('assets/models/scene1.gltf', ({ scene }) => {
      // console.log(scene);
      this.model = scene;
      // gltf.scenes[0]
      // scene.scale.set(0.1, 0.1, 0.1);
      scene.traverse((obj) => {
        if (obj.isMesh) {
          console.log(obj);
          obj.castShadow = true;
          obj.receiveShadow = true;
          if (obj.material) {
            obj.material.envMap = this.environment;
          }
          // obj.material = new MeshBasicMaterial({ wireframe: true });
        }
        if (obj.isLight) {
          obj.castShadow = true;
          obj.shadow.bias = -0.001;
          obj.shadow.radius = 8;
          obj.shadow.mapSize.width = 1024 * 2;
          obj.shadow.mapSize.height = 1024 * 2;
          obj.shadow.camera.near = 0.1;
          obj.shadow.camera.far = 100;
          console.log(obj);
        }
      });

      this.scene.add(scene);
    });

    // new FBXLoader().load('assets/models/scene3.fbx', (scene) => {
    //   console.log(scene);
    //   this.model = scene;
    //   // gltf.scenes[0]
    //   scene.scale.set(0.1, 0.1, 0.1);
    //   scene.traverse((obj) => {
    //     if (obj.isMesh) {
    //       // obj.material = new MeshBasicMaterial({ wireframe: true });
    //     }
    //   });
    //   this.scene.add(scene);
    // });

    this.render();
    window.addEventListener('resize', this.resize);
  }

  render() {
    this.controls.update();
    this.cube.rotation.y += 0.01;
    this.renderer.render(this.scene, this.camera);
    requestAnimationFrame(this.render);
  }

  resize() {
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
  }
}

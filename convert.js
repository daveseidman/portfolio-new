const chokidar = require('chokidar');
// const convert = require('@cocos/fbx2gltf');
const convert = require('fbx2gltf');

const folder = './etc/convert';
const path = require('path');

const watcher = chokidar.watch(path.resolve(folder), { ignored: /^\./, persistent: true });

console.log('watching for .fbx files in the folder', path.resolve(folder));

const debounce = (func, wait, immediate) => {
  let timeout;
  return function () {
    const context = this; const
      args = arguments;
    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};
const convertFile = (path) => {
  if (path.indexOf('.fbx') >= 0 && path.indexOf('.bak') === -1) {
    console.log(`converting ${path} to ${path.replace('fbx', 'glb')}`);
    convert(path, path.replace('fbx', 'glb'), ['--embed']);
  }
};

const convertFileDebounced = debounce(convertFile, 2000);

watcher
  .on('add', convertFileDebounced)
  .on('change', convertFileDebounced);
